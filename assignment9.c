#include <stdio.h>
#include <conio.h>
void display(struct emp e1);
typedef struct
{
   
    int dd;
    int mm;
    int yyyy;
}DATE;
struct emp
{
    int empid;
    char name[30];
    int age;
    int sal;
    DATE dob;
    float exp;
};
void read(struct emp e1)
{
    printf("Enter the employee ID :\n");
    scanf("%d",&e1.empid);
    printf("Enter the employee name :\n");
    scanf("%s",e1.name);
    printf("Enter the employee age :\n");
    scanf("%d",&e1.age);
    printf("Enter the date of birth in dd/mm/yyyy format :\n");
    scanf("%d/%d/%d",&e1.dob.dd, &e1.dob.mm, &e1.dob.yyyy);
    printf("Enter the employee salary :\n");
    scanf("%d",&e1.sal);
    printf("Enter the experience of the employee in years :\n");
    scanf("%f",&e1.exp);
    display(e1);
}
void display(struct emp e1)  
{
    printf("\n------EMPLOYEE DETAILS------\n");
    printf("\nEmployee ID : %d\n",e1.empid);
    printf("Employee Name : %s\n",e1.name);
    printf("Employee Age : %d\n",e1.age);
    printf("Employee Date of Birth : %d/%d/%d\n", e1.dob.dd, e1.dob.mm, e1.dob.yyyy);
    printf("Employee Salary : %d\n",e1.sal);
    printf("Employee Experience : %0.2f\n",e1.exp);
    printf("\n----------------------------\n");
}
int main()
{
    struct emp e1;
    read(e1);
    return 0;
    
}