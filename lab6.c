#include <stdio.h>
int read()
{
    int n;
    scanf("%d",&n);
    return n;
}
int main()
{
    int a[10][10], t[10][10],r,c,i,j;
    printf("Enter the number of rows of the matrix : \n");
    r=read();
    printf("Enter the number of columns of the matrix : \n");
    c=read();
    for(i=0;i<r;i++)
    {
        for(j=0;j<c;j++)
        {
            printf("a[%d][%d]=",i+1,j+1);
            scanf("%d",&a[i][j]);
        }
    }
    printf("The entered matrix is : \n");
    for(i=0;i<r;i++)
    {
        for(j=0;j<c;j++)
        {
            printf("%d  ",a[i][j]);
            if(j==c-1)
            printf("\n");
        }
    }
    for(i=0;i<r;i++)
    {
        for(j=0;j<c;j++)
        {
            t[j][i]=a[i][j];
        }
    }
    printf("The TRANSPOSE of the matrix is : \n");
    for(i=0;i<c;i++)
    {
        for(j=0;j<r;j++)
        {
            printf("%d  ",t[i][j]);
            if(j==r-1)
            printf("\n");
        }
    }
    return 0;
}